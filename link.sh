#!/bin/sh

## This script links my static pages to /var/www/html

rm -rf /var/www/html

echo "Cleaned up /var/www/html"

cp -r ./html /var/www/html
